package com.asm.smarthome.coremodels.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "devices")
public class Device {

    @Id
    private ObjectId ref;

    public ObjectId getRef() {
        return ref;
    }

    public void setRef(ObjectId ref) {
        this.ref = ref;
    }
}
